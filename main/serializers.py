from .models import Client, Mailing
from rest_framework import serializers, fields
import datetime


# Default serializer for Client model
class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = '__all__'


# Custim serializer for Mailing model
class MailingSerializer(serializers.ModelSerializer):

    # Validation for 'stop' field
    def validate_stop(self, value):
        format = "%Y-%m-%dT%H:%M:%S%z"
        if value:
            if value <= datetime.datetime.strptime(self.initial_data.get("start"), format):
                raise serializers.ValidationError("Stop timestamp should be later than start timestamp")
        return value

    class Meta:
        model = Mailing
        fields = ['pk', 'start', 'text', 'stop', 'filter_tag', 'filter_code']
