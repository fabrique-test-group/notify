from notify.celery import app
import requests
import json
from .models import Message, Mailing
import pytz
import datetime

# Celery task
@app.task()
def make_message(id, phone, text):

    # Initialize HTTP Post request to message sending API
    url = 'https://probe.fbrq.cloud/v1/' + str(id)
    data = {
        "id": id,
        "phone": phone,
        "text": text
    }
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2OTI5NjA1NzQsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6IkRfS0VMUlkifQ.bzcKZ1-AOOCsKcTmRP0ad20irfS-HdNUG6M_End83UQ'
    }
    r = requests.post(url, data=json.dumps(data), headers=headers)

    msg = Message.objects.get(pk=id)
    mailing = msg.mailing

    # Parse server's answer
    try:
        answer = json.loads(r.text)
        if answer.code == 0:
            if mailing.stop:
                if mailing.stop < pytz.utc.localize(datetime.datetime.utcnow()):
                    msg.status = 'OK'
                else:
                    msg.status = 'ERROR'
            else:
                msg.status = 'OK'
        else:
            msg.status = 'ERROR'
    except:
        msg.status = 'ERROR'

    msg.save()
