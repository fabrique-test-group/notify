from django.contrib import admin
from .models import Client, Mailing, Message


class ClientAdmin(admin.ModelAdmin):
    list_display = ['pk', 'phone', 'op_code', 'tag', 'timezone']
admin.site.register(Client, ClientAdmin)

class MailingAdmin(admin.ModelAdmin):
    list_display = ['pk', 'start', 'text', 'stop', 'filter_tag', 'filter_code']
admin.site.register(Mailing, MailingAdmin)

class MessageAdmin(admin.ModelAdmin):
    list_display = ['pk', 'created', 'status', 'mailing', 'client']
admin.site.register(Message, MessageAdmin)
