from django.db import models
from django.core import validators
import pytz

# Client model
class Client(models.Model):

    TIMEZONES = tuple(zip(pytz.all_timezones, pytz.all_timezones))

    phone = models.CharField(null=False, blank=False, max_length=11,
                             validators=[validators.RegexValidator(regex='7\d{10}', message='Invalid phone format')])
    op_code = models.CharField(max_length=3, default='000', null=False, blank=False,
                               validators=[validators.RegexValidator(regex='\d{3}', message='Bad phone code')])

    tag = models.CharField(null=True, blank=True, max_length=50)
    timezone = models.CharField(max_length=32, choices=TIMEZONES, default='UTC')


# Mailing model
class Mailing(models.Model):

    start = models.DateTimeField(null=False, blank=False)
    text = models.TextField(max_length=100)
    stop = models.DateTimeField(null=True, blank=True)
    filter_tag = models.CharField(null=True, blank=True, max_length=50)
    filter_code = models.CharField(null=True, blank=True, max_length=3,
                                   validators=[validators.RegexValidator(regex='\d{3}', message='Invalid code format')])


# Message model
class Message(models.Model):
    STATUSES = (
        ("OK", "Sent"),
        ("ERROR", "Error"),
        ("PEND", "Pending")
    )

    created = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=5, choices=STATUSES, default="PEND")
    mailing = models.ForeignKey(Mailing, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    task_id = models.UUIDField(primary_key=False, editable=True, null=True, blank=True)
