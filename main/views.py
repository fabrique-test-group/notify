from django.shortcuts import render
from .serializers import ClientSerializer, MailingSerializer
from .models import Client, Mailing, Message
from rest_framework.viewsets import ModelViewSet
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .tasks import make_message
import datetime
import pytz
import uuid
from celery.task.control import revoke
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q


# Meta API View Set for Client model
class APIClientsViewSet(ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


# Custom API View for Mailing model (list)
class APIMailingsView(APIView):
    # Retrieve list of Mailings
    def get(self, request):
        mailings = Mailing.objects.all()
        serializer = MailingSerializer(mailings, many=True)
        return Response(serializer.data)

    # Add new Mailing
    def post(self, request):
        serializer = MailingSerializer(data=request.data)

        if serializer.is_valid():
            instance = serializer.save()

            clients = Client.objects.all()
            if instance.filter_tag:
                clients = Client.objects.filter(tag=instance.filter_tag)
            if instance.filter_code:
                clients = Client.objects.filter(op_code=instance.filter_code)

            if instance.stop > pytz.utc.localize(datetime.datetime.utcnow()):

                for client in clients:
                    msg = Message.objects.create(
                        status='OK',
                        mailing=instance,
                        client=client
                    )
                    msg.save()
                    if instance.start < pytz.utc.localize(datetime.datetime.utcnow()):
                        res = make_message.apply_async(eta=datetime.datetime.utcnow(), args=[msg.pk, client.phone, instance.text])
                    else:
                        res = make_message.apply_async(eta=instance.start, args=[msg.pk, client.phone, instance.text])
                    msg.task_id = uuid.UUID(res.id)
                    msg.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# Custom API View for Mailing model (detail)
class APIMailingView(APIView):

    # Edit existing mailing
    def put(self, request, pk):
        try:
            instance = Mailing.objects.get(pk=pk)
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        serializer = MailingSerializer(instance, data=request.data)
        if serializer.is_valid():
            msgs = instance.message_set.all()

            for msg in msgs:
                old_id = msg.task_id
                if old_id:
                    revoke(old_id, terminate=True)
                msg.delete()

            serializer.save()

            clients = Client.objects.all()
            if instance.filter_tag:
                clients = Client.objects.filter(tag=instance.filter_tag)
            if instance.filter_code:
                clients = Client.objects.filter(op_code=instance.filter_code)

            if instance.stop > pytz.utc.localize(datetime.datetime.utcnow()):
                for client in clients:
                    msg = Message.objects.create(
                        status='OK',
                        mailing=instance,
                        client=client
                    )
                    if instance.start < pytz.utc.localize(datetime.datetime.utcnow()):
                        res = make_message.apply_async(eta=datetime.datetime.utcnow(), args=[msg.pk, client.phone, instance.text])
                    else:
                        res = make_message.apply_async(eta=instance.start, args=[msg.pk, client.phone, instance.text])
                    msg.task_id = uuid.UUID(res.id)
                    msg.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # Delete mailing
    def delete(self, request, pk):
        try:
            instance = Mailing.objects.get(pk=pk)
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        msgs = instance.message_set.all()

        for msg in msgs:
            old_id = msg.task_id
            if old_id:
                revoke(old_id, terminate=True)
        instance.delete()
        return Response(status=status.HTTP_200_OK)

    # Get stats on existing mailing
    def get(self, request, pk):
        try:
            instance = Mailing.objects.get(pk=pk)
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        data = {}
        messages = instance.message_set.all()
        data['amount'] = messages.count()
        data['amount_delivered'] = messages.filter(status='OK').count()
        data['amount_failed'] = messages.filter(~Q(status='OK')).count()
        data['messages'] = messages
        return Response(data, status=status.HTTP_200_OK)


# Custom API View for basic statistics
class APIMailingsStatsView(APIView):
    def get(self, request):
        mailings = Mailing.objects.all()

        # Get stats for each mailing instance
        data = []
        for instance in mailings:
            mailing = {
                'mailing': {
                    'pk': instance.pk,
                    'start': instance.start,
                    'stop': instance.stop,
                    'text': instance.text,
                    'filter_tag': instance.filter_tag,
                    'filter_code': instance.filter_code
                }
            }

            # Get basic stats on messages of each mailing
            messages = instance.message_set.all()
            mailing['msg_amount'] = messages.count()
            mailing['msg_amount_delivered'] = messages.filter(status='OK').count()
            mailing['msg_amount_failed'] = messages.filter(~Q(status='OK')).count()
            data.append(mailing)
        return Response(data, status=status.HTTP_200_OK)
