from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import APIClientsViewSet, APIMailingsView, APIMailingView, APIMailingsStatsView

# Create default routes for APIViewSet (Clients)
router = DefaultRouter()
router.register('clients', APIClientsViewSet)

# Set up API routes
urlpatterns = [
    path('', include(router.urls)),
    path('mailings/', APIMailingsView.as_view()),
    path('mailings/<int:pk>/', APIMailingView.as_view()),
    path('mailings/stats/', APIMailingsStatsView.as_view())
]
