# Generated by Django 3.2.15 on 2022-08-26 16:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0010_alter_client_op_code'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='status',
            field=models.CharField(choices=[('OK', 'Sent'), ('ERROR', 'Error'), ('PEND', 'Pending')], default='OK', max_length=5),
        ),
    ]
